import { Input, Component } from "@angular/core";
import { IIngredientBar } from "../interfaces";

@Component({
    selector: 'app-ingredient-bar',
    templateUrl: "./ingredient-bar.component.html",
    styleUrls: ['./ingredient-bar.component.scss'],
})

export class IngredientBarComponent {

    @Input() backgroundColorLightGreen = false;

    @Input() cmsData: IIngredientBar;

    constructor() { }
}
