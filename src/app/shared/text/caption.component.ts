import { Component, Input } from '@angular/core';
import { GenericTextComponent } from './generic-text.component';

@Component({
    selector: 'app-caption',
    template: `
        <p
            class="caption"
            [class.light]="light"
            [class.inline]="display === 'inline'"
            [class.inline-block]="display === 'inline-block'"
            [class.block]="display === 'block'"
            [class.bold]="bold"
            [class.vertical-margin]="verticalMargin"
            [class.vertical-margin-small]="verticalMarginSmall"
        >
            <ng-content></ng-content>
        </p>
    `,
    styleUrls: ['./text.component.scss'],
})
export class CaptionComponent extends GenericTextComponent {
    @Input() verticalMargin = false;
    @Input() verticalMarginSmall = false;
}
