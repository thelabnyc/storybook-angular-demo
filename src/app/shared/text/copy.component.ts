import { Component } from '@angular/core';
import { GenericTextComponent } from './generic-text.component';

@Component({
    selector: 'app-copy',
    template: `
        <ng-template #innerTemplate><ng-content></ng-content></ng-template>
        <ng-container *ngIf="animate; else noAnimation">
            <p
                class="copy"
                [class.light]="light"
                [class.block]="display === 'block'"
                [class.inline-block]="display === 'inline-block'"
                [class.inline]="display === 'inline'"
                [class.vertical-margin]="verticalMargin"
                [class.vertical-margin-small]="verticalMarginSmall"
                [class.bold]="bold"
                animateOnScroll
                animationName="a-fade-in"
            >
                <ng-container *ngTemplateOutlet="innerTemplate"></ng-container>
            </p>
        </ng-container>
        <ng-template #noAnimation>
            <p
                class="copy"
                [class.light]="light"
                [class.block]="display === 'block'"
                [class.inline-block]="display === 'inline-block'"
                [class.inline]="display === 'inline'"
                [class.bold]="bold"
                [class.vertical-margin]="verticalMargin"
                [class.vertical-margin-small]="verticalMarginSmall"
            >
                <ng-container *ngTemplateOutlet="innerTemplate"></ng-container>
            </p>
        </ng-template>
    `,
    styleUrls: ['./text.component.scss'],
})
export class CopyComponent extends GenericTextComponent {}
