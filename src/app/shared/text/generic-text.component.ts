import { Input } from '@angular/core';

type DisplayType = 'block' | 'inline-block' | 'inline';

export class GenericTextComponent {
    @Input() light = false;
    @Input() display: DisplayType = 'block';
    @Input() verticalMargin = false;
    @Input() verticalMarginSmall = false;
    @Input() bold = false;

    /**
     * If true, fade in when the users scrolls into view of the element
     */
    @Input() animate = false;
}
