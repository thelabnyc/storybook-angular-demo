import { Component } from '@angular/core';

@Component({
    selector: 'app-quote',
    template: `
        <blockquote class="quote">
            <ng-content></ng-content>
        </blockquote>
    `,
    styleUrls: ['./text.component.scss'],
})
export class QuoteComponent {}
