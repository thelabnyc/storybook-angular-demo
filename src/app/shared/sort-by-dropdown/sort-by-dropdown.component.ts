import { Component, Input } from '@angular/core';

interface Items {
    label: string;
}

@Component({
    selector: 'app-sort-by-dropdown',
    templateUrl: './sort-by-dropdown.component.html',
    styleUrls: ['./sort-by-dropdown.component.scss'],
})
export class SortByDropdownComponent {
    /**
     * Not sure if this is the data model that this component needs;
     * refactor as necessary
     */
    @Input() items: Items[] = [];
    @Input() title: string;
    /** Select a single item or multiple items */

    selectedItems: string[];
}
