import { Input, HostBinding } from '@angular/core';

type DisplayType = 'block' | 'inline-block' | 'inline';
export type TagType = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'div' | 'span' | 'p';
export type ThemeType = 'h1' | 'h2' | 'h3' | 'h4' | 'h5';

export class GenericHeaderComponent {
    /**
     * Shows a lighter color for cases where the background is darker.
     */
    @Input() light = false;

    /**
     * Design doc specified a default margin, so it's available here.
     */
    @Input() verticalMargin = false;

    @Input() verticalMarginSmall = false;

    /**
     * Using a subset of CSS display here. Add other types if common use cases
     * require it.
     * Headers are block by default.
     */
    @Input() display: DisplayType = 'block';

    /**
     * Sometimes the visual appearance of a header does not match the desired
     * semantic structure. This allows you to change the HTML tag used.
     */
    @Input() tag: TagType;

    /**
     * If a dev did something like <app-h1 class="foo">, the HostBinding getter
     * would override it. This adds it back.
     */
    @Input() class: string;

    /**
     * If true, fade in when the users scrolls into view of the element
     */
    @Input() animate = false;

    /**
     * I wanted to do something like:
     * @HostBinding('class.vertical-margin') @Input() verticalMargin = false;
     * but had problems with not all of the classes showing in the DOM. Went
     * with a getter instead.
     */
    @HostBinding('class') get valid() {
        const classes: string[] = [];
        if (this.light) {
            classes.push('light');
        }
        if (this.display) {
            classes.push(this.display);
        }
        if (this.verticalMargin) {
            classes.push('vertical-margin');
        }
        if (this.verticalMarginSmall) {
            classes.push('vertical-margin-small');
        }
        if (this.class) {
            classes.push(this.class);
        }
        return classes.join(' ');
    }
}
