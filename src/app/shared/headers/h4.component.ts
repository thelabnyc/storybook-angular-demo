import { Component, Input } from '@angular/core';
import {
    GenericHeaderComponent,
    TagType,
    ThemeType,
} from './generic-header.component';

@Component({
    selector: 'app-h4',
    templateUrl: './headers.component.html',
    styleUrls: ['./headers.component.scss'],
})
export class H4Component extends GenericHeaderComponent {
    @Input() tag: TagType = 'h4';
    theme: ThemeType = 'h4';
}
