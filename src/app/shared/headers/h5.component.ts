import { Component, Input } from '@angular/core';
import {
    GenericHeaderComponent,
    TagType,
    ThemeType,
} from './generic-header.component';

@Component({
    selector: 'app-h5',
    templateUrl: './headers.component.html',
    styleUrls: ['./headers.component.scss'],
})
export class H5Component extends GenericHeaderComponent {
    @Input() tag: TagType = 'h5';
    theme: ThemeType = 'h5';
}
