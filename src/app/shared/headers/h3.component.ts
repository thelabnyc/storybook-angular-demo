import { Component, Input } from '@angular/core';
import {
    GenericHeaderComponent,
    TagType,
    ThemeType,
} from './generic-header.component';

@Component({
    selector: 'app-h3',
    templateUrl: './headers.component.html',
    styleUrls: ['./headers.component.scss'],
})
export class H3Component extends GenericHeaderComponent {
    @Input() tag: TagType = 'h3';
    theme: ThemeType = 'h3';
}
