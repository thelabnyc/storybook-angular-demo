import { Component, Input } from '@angular/core';
import {
    GenericHeaderComponent,
    TagType,
    ThemeType,
} from './generic-header.component';

@Component({
    selector: 'app-h1',
    templateUrl: './headers.component.html',
    styleUrls: ['./headers.component.scss'],
})
export class H1Component extends GenericHeaderComponent {
    @Input() tag: TagType = 'h1';
    theme: ThemeType = 'h1';
}
