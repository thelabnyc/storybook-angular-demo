import { Component, Input } from '@angular/core';
import {
    GenericHeaderComponent,
    TagType,
    ThemeType,
} from './generic-header.component';

@Component({
    selector: 'app-h2',
    templateUrl: './headers.component.html',
    styleUrls: ['./headers.component.scss'],
})
export class H2Component extends GenericHeaderComponent {
    @Input() tag: TagType = 'h2';
    theme: ThemeType = 'h2';
}
