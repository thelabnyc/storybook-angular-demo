export interface ITextBlock {
    title: string;
    subtitle: string;
    cta1: string;
    cta_link1: ILinkSnippet;
    cta2: string | null;
    cta_link2: ILinkSnippet | null;
    light_mode: boolean;
}

export interface IStreamfieldBlock<Type extends string, Data extends {}> {
    id: string;
    type: Type;
    value: Data;
}

export interface IImageBlock {
    image: string;
    alt_text: string;
}

export interface IInsiderBanner {
    text: string;
    cta_text: string;
    cta_link: string;
}

export interface ILink {
    text: string;
    html_title: string;
    open_in_new_tab: boolean;
    page: number | null;
    external_url: string;
}

export interface ICTA {
    style: string;
    link: ILink;
}

export interface IIngredientModule {
    text_block: ITextBlock;
    image_block: IImageBlock;
}

export interface ISideImage {
    image: string;
    text: string;
    cta: ICTA;
}

export interface IAdvert {
    orientation: string;
    image: string;
    side_image: ISideImage;
    text_block: any[];
}

export interface ILinkSnippet {
    url: string;
}

export interface IIngredientBar {
    icon1: boolean;
    icon2: boolean;
    icon3: boolean;
    icon4: boolean;
    icon5: boolean;
}

export type Height = 'none' | 'small' | 'medium' | 'large';
export type BackgroundColor = 'light-green' | 'white' | 'transparent';
export type FlexAlign = 'flex-start' | 'center' | 'flex-end';
export type BorderColor = 'nobel' | 'black';

export interface ISpacerModule {
    background_color: BackgroundColor;
    height: Height;
}
export type ISpacerStreamfieldBlock = IStreamfieldBlock<
    'spacer',
    ISpacerModule
>;

export interface IhrModule {
    content: boolean;
    height: Height;
    align: FlexAlign;
    bar_color: BorderColor;
    background_color: BackgroundColor;
}
export type IHrStreamfieldBlock = IStreamfieldBlock<
    'horizontal_rule',
    IhrModule
>;

export interface IBreadcrumb {
    text: string;
    link: string;
}

export interface IAncestor {
    id: number;
    title: string;
    slug: string;
}

export enum Alignment {
    left = 'left',
    right = 'right',
    center = 'center',
}
