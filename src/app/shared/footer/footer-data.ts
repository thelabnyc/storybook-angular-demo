import { IIngredientBar } from "../interfaces";

export const footerData = {
  ingredients: {
    icon1: true,
    icon2: true,
    icon3: true,
    icon4: false,
    icon5: false
  } as IIngredientBar,
  section1: {
    title: "Customer Service",
    items: [
      {
        text: "Curl Concierge",
        link: "#link"
      },
      {
        text: "FAQs",
        link: "#link"
      },
      {
        text: "Shipping & Returns",
        link: "#link"
      }
    ]
  },
  section2: {
    title: "Products",
    items: [
      {
        text: "Cleansers",
        link: "#link"
      },
      {
        text: "Conditioners",
        link: "#link"
      },
      {
        text: "Treatments",
        link: "#link"
      },
      {
        text: "Stylers",
        link: "#link"
      },
      {
        text: "Tools",
        link: "#link"
      },
      {
        text: "All Products",
        link: "#link"
      }
    ]
  },
  section3: {
    title: "About Us",
    items: [
      {
        text: "Our Story",
        link: "#link"
      },
      {
        text: "Stylist Finder",
        link: "#link"
      },
      {
        text: "Store Locator",
        link: "#link"
      },
      {
        text: "Careers",
        link: "#link"
      }
    ]
  },
  section4: {
    title: "Professionals",
    items: [
      {
        text: "Get Certified",
        link: "#link"
      },
      {
        text: "Academy",
        link: "#link"
      },
      {
        text: "Pro Resources",
        link: "#link"
      }
    ]
  },
  section5: {
    title: "Get 2 free samples with your first order"
  },
  section6: {
    items: [
      {
        text: "Privacy Policy",
        link: "#link"
      },
      {
        text: "Terms of Use",
        link: "#link"
      },
      {
        text: "Accessibility",
        link: "#link"
      }
    ]
  }
};
