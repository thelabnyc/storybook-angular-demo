import { Component, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'app-button-unstyled',
    template: `
        <button><ng-content></ng-content></button>
    `,
    styleUrls: ['./button-unstyled.component.scss'],
})
export class ButtonUnstyledComponent {
    @Output() click = new EventEmitter();
}
