import {
    Component,
    ChangeDetectionStrategy,
    EventEmitter,
    Input,
    Output,
} from '@angular/core';

type ClickableTheme =
    | 'button'
    | 'button-flashy'
    | 'text-link'
    | 'tabs'
    | 'dropdown'
    | 'unstyled';
type DisplayType = 'block' | 'inline-block' | 'inline';

@Component({
    selector: 'app-clickable',
    templateUrl: './clickable.component.html',
    styleUrls: ['./clickable.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClickableComponent {
    /**
     * Wanted to pick a default, so it's button. Hope that was a good call.
     */
    @Input() theme: ClickableTheme = 'button';

    /**
     * Using a subset of CSS display here. Add other types if common use cases
     * require it.
     * Sets sensible defaults for different themes.
     */
    @Input() display: DisplayType = this.theme.includes('button')
        ? 'block'
        : 'inline';

    /**
     * Text link hover state can also be an active state in tab situations
     * I wanted to set a default value to false here, but I also wanted text-link
     * theme to default to true, and doing that here with a ternary didn't work
     * for some reason. So I did it in the template in a sorta wacky way.
     */
    @Input() active;

    /**
     * Design doc specified a default margin, so it's available here.
     */
    @Input() verticalMargin = false;
    @Input() verticalMarginSmall = false;

    /**
     * Sets the width to 100% so it can fill the width of its container
     */
    @Input() fullWidth = false;
    @Input() fullWidthMobile = false;

    /**
     * Sets the `tabindex` attribute of the clickable inside.
     * While it would be more intuitive to name this Input `tabindex`, that would
     * add tabindex the <app-clickable> host element as well, which seems bad
     * For what it's worth, WebAIM recommends avoiding positive tabindex values:
     * https://webaim.org/techniques/keyboard/tabindex
     */
    @Input() clickableTabIndex: number;

    /**
     * If it has a href or routerLink, it'll be an <a> element. If not, it'll be a <button>.
     */
    @Input() href: string;
    @Input() target: string | undefined;
    @Input() routerLink: string | any[];
    @Input() queryParams: { [k: string]: any };
    @Input() light = false;
    @Input() greenButton = false;

    @Output() click = new EventEmitter();
}
