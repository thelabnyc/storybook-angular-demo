import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

type cropOption =
    | 'scale'
    | 'fit'
    | 'mfit'
    | 'fill'
    | 'lfill'
    | 'limit'
    | 'pad'
    | 'lpad'
    | 'mpad'
    | 'crop'
    | 'thumb'
    | 'imagga_crop'
    | 'imagga_scale';

type gravityOption = 'face' | 'custom';

@Component({
    selector: 'app-image',
    template: `
        <cl-image
            [public-id]="publicId"
            [attr.alt]="alt"
            [attr.aspect_ratio]="aspect_ratio"
            [attr.width]="width"
            [attr.height]="height"
            [attr.crop]="crop"
            [attr.effect]="effect"
            [attr.gravity]="gravity"
            [attr.radius]="radius"
            [attr.zoom]="zoom"
            [attr.x]="x"
            [attr.y]="y"
            quality="auto"
            [attr.fetch-format]="fetch()"
        >
        </cl-image>
    `,
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageComponent {
    @Input() publicId: string;
    @Input() alt: string;
    @Input() width: string;
    @Input() height: string;
    @Input() aspect_ratio: string;
    @Input() effect: string;
    @Input() fetchFormat: boolean;
    @Input() gravity: gravityOption;
    @Input() radius: string;
    @Input() zoom: string;
    @Input() x: string;
    @Input() y: string;
    private _crop: cropOption;

    // fetchFormat should be set to false when the image being import is or might be an SVG

    fetch() {
        if (this.fetchFormat === false) {
            return '';
        } else {
            return 'auto';
        }
    }

    /**
     * Setting width requires either a crop or a aspect ratio
     * However cloudinary_angular has a bug where this isn't so. It requires a crop.
     * Therefore we must set a custom crop when width is used.
     */
    get crop(): cropOption {
        if (this.width) {
            if (this._crop) {
                return this._crop;
            } else {
                return 'fill'; // Default when width is set
            }
        }
        return this._crop;
    }

    @Input() set crop(value: cropOption) {
        this._crop = value;
    }
}
