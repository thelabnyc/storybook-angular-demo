import {
    Component,
    Input,
    ChangeDetectionStrategy,
    EventEmitter,
    Output,
} from '@angular/core';

interface Items {
    label: string;
    icon?: string;
}

@Component({
    selector: 'app-dropdown',
    templateUrl: './dropdown.component.html',
    styleUrls: ['./dropdown.component.scss'],
    changeDetection: ChangeDetectionStrategy.Default,
})
export class DropdownComponent {
    @Input() items: Items[] = [];
    @Input() selectedItems: Items[];
    @Input() title: string;
    /** Select a single item or multiple items */
    @Input() multiple = false;
    @Input() bindValue = 'label';
    @Output() change = new EventEmitter();

    isOpen = false;

    onOpen() {
        this.isOpen = true;
    }

    onClose() {
        this.isOpen = false;
    }
}
