/* tslint:disable:component-selector */
import {
    Component,
    Input,
    PLATFORM_ID,
    Inject,
    AfterViewInit,
    ElementRef,
    EventEmitter,
    Output,
    ViewChild,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Options } from 'flickity';

declare var Flickity;

/** Danger! This might break if staticClick's typing ever changes */
export interface IStaticClickEvent {
    event?: Event;
    pointer?: Element | Touch;
    cellElement?: Element;
    cellIndex?: number;
}

@Component({
    selector: 'flickity-carousel',
    template: `
        <div #carousel class="dark">
            <ng-content></ng-content>
        </div>
    `,
    styleUrls: ['./base-flickity.scss'],
})
export class FlickityCarousel implements AfterViewInit {
    public carousel: any;
    @Input() options?: Options;
    @Output() change = new EventEmitter();
    @Output() staticClick = new EventEmitter<IStaticClickEvent>();
    @ViewChild('carousel', { static: true }) container: ElementRef;

    constructor(@Inject(PLATFORM_ID) private platformId: Object) {}

    ngAfterViewInit() {
        if (isPlatformBrowser(this.platformId)) {
            const options: Options = {
                // default options go here
                watchCSS: true,
                wrapAround: true,
                // lazyLoad: 1,
                ...this.options, // doesn't error if undefined, thanks JS
                on: {
                    change: index => this.change.emit(index),
                    staticClick: (event, pointer, cellElement, cellIndex) =>
                        this.staticClick.emit({
                            event,
                            pointer,
                            cellElement,
                            cellIndex,
                        }),
                },
            };

            this.carousel = new Flickity(this.container.nativeElement, options);
            this.carousel.resize(); // for some reason this fixes some problems that show up with watchCSS
        }
    }
}
